import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

// queue node used in BFS
class Node {
    // (x, y) represents matrix cell coordinates
    // dist represent its minimum distance from the source
    int x, y, dist;

    Node(int x, int y, int dist) {
        this.x = x;
        this.y = y;
        this.dist = dist;
    }
}

public class LeeAlgorithm {
    private int M = 10;
    private int N = 10;
    private int FINAL_MARK;

    private int[][] mat =
            {
                    {1, 1, 1, 1, 1, 0, 0, 1, 1, 1},
                    {0, 1, 1, 1, 1, 1, 0, 1, 0, 1},
                    {0, 0, 1, 0, 1, 1, 1, 0, 0, 1},
                    {1, 0, 1, 1, 1, 0, 1, 1, 0, 1},
                    {0, 0, 0, 1, 0, 0, 0, 1, 0, 1},
                    {1, 0, 1, 1, 1, 0, 0, 1, 1, 0},
                    {0, 0, 0, 0, 1, 0, 0, 1, 0, 1},
                    {0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                    {1, 1, 1, 1, 1, 0, 0, 1, 1, 1},
                    {0, 0, 1, 0, 0, 1, 1, 0, 0, 1}
            };

    LeeAlgorithm() {
    }

    LeeAlgorithm(int n, int m, int[][] mat) {
        this(n, m, 7, mat);
    }

    LeeAlgorithm(int n, int m, int finalMark, int[][] mat) {
        N = n;
        M = m;
        FINAL_MARK = finalMark;
        this.mat = mat;
    }


    // Below arrays details all 4 possible movements from a cell
    private final int row[] = {-1, 0, 0, 1};
    private final int col[] = {0, -1, 1, 0};

    private boolean isFinalDestination(int mat[][], boolean visited[][], int row, int col) {
        return mat[row][col] == FINAL_MARK && !visited[row][col];
    }

    // Function to check if it is possible to go to position (row, col)
    // from current position. The function returns false if (row, col)
    // is not a valid position or has value 0 or it is already visited
    private boolean isValidNextPosition(int mat[][], boolean visited[][], int row, int col) {
        return mat[row][col] == 1 && !visited[row][col];
    }

    // Find Shortest Possible Route in a matrix mat from source
    // cell (i, j) to destination cell (x, y)
    public void BFS(int mat[][], int i, int j, int x, int y) {
        boolean[][] visited = new boolean[M][N];
        Queue<Node> q = new ArrayDeque<>();

        // mark source cell as visited and enqueue the source node
        visited[i][j] = true;
        q.add(new Node(i, j, 0));

        // stores length of longest path from source to destination
        int min_dist = Integer.MAX_VALUE;

        while (!q.isEmpty()) {
            Node node = q.poll();
            i = node.x;
            j = node.y;
            int dist = node.dist;

            //check if position indicates final destination
            if (i == x && j == y) {
                min_dist = dist;
                break;
            }

            // check for all 4 possible movements from current cell
            for (int k = 0; k < 4; k++) {
                // check if it is possible to go to position
                int nextRow = i + row[k];
                int nextColumn = j + col[k];
                if ((nextRow >= 0) && (nextRow < M) && (nextColumn >= 0) && (nextColumn < N)) {
                    if (isValidNextPosition(mat, visited, nextRow, nextColumn)) {
                        // mark next cell as visited and enqueue it
                        visited[nextRow][nextColumn] = true;
                        q.add(new Node(nextRow, nextColumn, dist + 1));
                    }
                    //check if containing value indicates final destination
                    else if (isFinalDestination(mat, visited, nextRow, nextColumn)) {
                        printMinDist(++dist);
                        return;
                    }
                }
            }
        }

        printMinDist(min_dist);
    }

    private void printMinDist(int min_dist) {
        if (min_dist != Integer.MAX_VALUE) {
            System.out.print("The shortest path from source to destination "
                    + "has length " + min_dist);
        } else {
            System.out.print("Destination can't be reached from source");
        }
    }

    public void run() {
        BFS(mat,0,0, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public void run(int startRow, int startColumn, int endRow, int endColumn) {
        BFS(mat, startRow, startColumn, endRow, endColumn);
    }

    private static void printMatrix(int numRows, int numColumns, List<List<Integer>> matrix) {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++)
                System.out.println(matrix.get(i).get(j) + " ");
            System.out.println();
        }
        System.out.println();
    }

}