import java.util.Arrays;
import java.util.List;


public class Main {

    List<List<Integer>> matrice = Arrays.asList(
            Arrays.asList(1, 0, 0),
            Arrays.asList(1, 0, 0),
            Arrays.asList(1, 7, 1)
    );


    public static void main(String[] args) {
//        BitsXORAlgorithm.cellCompete(new int[]{1, 1, 1, 0, 1, 1, 1, 1}, 2).forEach(System.out::println);
//        System.out.println(GCDArray.generalizedGCD(5, new int[]{20,4,64,80,12}));
//        new LeeAlgorithm().run(0, 0, 7, 5);

        int matrix1[][] = {
                {1,0,0},
                {1,0,0},
                {1,9,0}
        };
        new LeeAlgorithm(3,3,matrix1).run();

    }

}


