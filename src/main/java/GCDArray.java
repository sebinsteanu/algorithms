import java.math.BigInteger;

public class GCDArray {


    public static int generalizedGCD(int size, int[] array) {
        int gcd = array[0];
        for (int i = 0; i < size; i++) {
            gcd = gcdComputation(gcd, array[i]);
        }
        return gcd;
    }

    private static int gcdComputation(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }

}
