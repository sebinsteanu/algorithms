import java.util.ArrayList;
import java.util.List;

/**
 * Eight cells, are arranged in a straight line.
 * Each day every cell competes with its adjacent cells.
 * An integer value of 1 represents an active cell and a value of 0 represents an inactive cell.
 * If the neighbors on both the sides of a cell are either active or inactive, the cell becomes inactive on the next day; otherwise the cell becomes active.
 * The two cells on each end have a single adjacent cell, so assume that the unoccupied space on the opposite side is an inactive cell.
 * Even after updating the cell state, consider its previous state when updating the state of other cells.
 * <p>
 * The state information of all cells should be updated simultaneously.
 * <p>
 * <p>
 * The input to the method consists of two arguments:
 * states - a list of integers representing the current state of cells;
 * days - an integer representing the number of days.
 * <p>
 * Return: a list of integers representing the state of the cells after the given number of days.
 * <p>
 * <p>
 * Input:
 * [1, 0, 0, 0, 0, 1, 0, 0], 1
 * Expected Return Value:
 * 0 1 0 0 1 0 1 0
 * <p>
 * Input:
 * [1, 1, 1, 0, 1, 1, 1, 1], 2
 * Expected Return Value:
 * 0 0 0 0 0 1 1 0
 */

public class BitsXORAlgorithm {
    private static byte getBit(int position, byte number) {
        return (byte) ((number >> position) & 0x1);
    }

    public static List<Integer> cellCompete(int[] states, int days) {
        int index = 0;
        byte statesByteValue = 0b0;
        while (index < states.length) {
            if (states[index] == 1) {
                statesByteValue = (byte) (statesByteValue + (1 << (states.length - index - 1)));
            }
            index++;
        }

        while (days-- > 0) {

            byte leftShift = (byte) ((byte) (statesByteValue << 1) & 0xff);
            byte rightShift = (byte) ((statesByteValue & 0xff) >>> 1);

            System.out.println("left:" + Integer.toBinaryString(leftShift & 0xff));
            System.out.println("right: " + Integer.toBinaryString(rightShift & 0xff));

            statesByteValue = (byte) (((byte) (leftShift ^ rightShift)) & 0xff);

        }
        System.out.println("xor:" + Integer.toBinaryString(statesByteValue & 0xff));

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < states.length; i++) {
            int bit = (int) getBit(states.length - i - 1, statesByteValue);
            result.add(i, bit);
        }
        return result;
    }
}
